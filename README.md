# Lua Script Extender

The Lua Script Extender is a library to be used together with software that relies on either [LuaJIT](http://luajit.org/) or [plain Lua](https://www.lua.org/), working fine with both. The name was inspired by the [Skyrim Script Extender](http://skse.silverlock.org/). Aside from Lua or LuaJIT, the other requirement will be a library that handles dynamic loading of libraries: for Unix-based systems, this is _ld_, which supplies the functions _dlopen_ and _dlsym_.

*The Lua Script Extender is intended to be a core part of the Haruka Engine.*

Mandatory functions for any library that would be compatible:
* ```const luaL_Reg* LIBREG()``` - returns a pointer, the address of a const luaL_Reg[], terminated by a final ```{ 0, 0 }```
* ```const char* LIBNAME()``` - returns a pointer, a null-terminated C-style string
* Both must have C-linkage
