#ifndef EXTENDER_HPP
#define EXTENDER_HPP
#include <lua.hpp>
typedef const char* __LIBNAME;
typedef const luaL_Reg* __LIBREG;
typedef __LIBNAME (*LIBNAME)(void);
typedef __LIBREG (*LIBREG)(void);
void registerExtension(lua_State *L, LIBNAME name, LIBREG registry);
void registerExtension(lua_State *L, void* dll);
void registerExtension(lua_State *L, const char* dllPath, bool lazyLoad=false);


#endif // EXTENDER_HPP
