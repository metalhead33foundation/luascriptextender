#include "extender.hpp"
#include <string>
#include <sstream>
#include <dlfcn.h>
#include <stdexcept>
#include <iostream>
void registerExtension(lua_State *L, LIBNAME name, LIBREG registry)
{
	std::string temp_metname = "__" + std::string(name());
	luaL_newmetatable(L, temp_metname.c_str());
	luaL_register(L, NULL, registry());
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, temp_metname.c_str());
	lua_setglobal(L, name());
}
void registerExtension(lua_State *L, void* dll)
{
	LIBNAME tName = reinterpret_cast<LIBNAME>(dlsym(dll, "LIBNAME"));
	if(!tName)
	{
		throw std::runtime_error("ERROR: Failed to load LIBNAME from the Lua extension library!");
	}
	else
	{
		std::cout << "Successfully found LIBNAME: [" << reinterpret_cast<const void*>(tName) << "] = " << tName() << std::endl;
	}
	LIBREG tReg = reinterpret_cast<LIBREG>(dlsym(dll, "LIBREG"));
	if(!tReg)
	{
		throw std::runtime_error("ERROR: Failed to load LIBREG from the Lua extension library!");
	}
	else
	{
		std::cout << "Successfully found LIBREG: [" << reinterpret_cast<const void*>(tReg) << "]." << std::endl;
	}
	registerExtension(L, tName, tReg);
}
void registerExtension(lua_State *L, const char* dllPath, bool lazyLoad)
{
	void* tDll;
	if(lazyLoad)
	{
		tDll = dlopen(dllPath,RTLD_LAZY | RTLD_LOCAL | RTLD_DEEPBIND);
	}
	else
	{
		tDll = dlopen(dllPath,RTLD_NOW | RTLD_LOCAL | RTLD_DEEPBIND);
	}
	if(!tDll)
	{
		std::ostringstream errorText;
		errorText << "Failed to open the library supposedly at path \"" << dllPath << "\".";
		throw std::runtime_error(errorText.str().c_str());
	}
	try
	{
		registerExtension(L, tDll);
	}
	catch(std::exception e)
	{
		std::ostringstream errorText;
		errorText << "An exception was caught while trying to open the library supposedly at path \"" << dllPath << "\":\n" << e.what();
		throw std::runtime_error(errorText.str().c_str());
	}
}
