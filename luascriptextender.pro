TEMPLATE = lib
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt
include(qmake_modules/findLuaJIT.pro)
include(qmake_modules/findLibraryLinker.pro)

SOURCES += \
    extender.cpp

HEADERS += \
    extender.hpp

DISTFILES += \
    README.md
